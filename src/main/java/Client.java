import java.util.Random;

class Client extends Thread {
    private final Warehouse warehouse;
    private final Integer number;

    Client(Warehouse warehouse, Integer number){
        this.warehouse = warehouse;
        this.number = number;
    }

    @Override
    public void run(){
        System.out.println("Client nr." + this.number + " is starting");
        Random generator = new Random();
        while (true){
            try {
                sleep((generator.nextInt(10) + 1) * 1000);
                Products product = Products.values()[generator.nextInt(Products.values().length)];
                int amount = generator.nextInt(5) + 1;
                System.out.println("Client nr." + this.number + " is trying to buy " + amount + " x " + product);
                boolean response = this.warehouse.SellProduct(product, amount);
                if(response){
                    System.out.println("Client nr." + this.number + " bought " + amount + " x " + product);
                }
                else{
                    System.out.println("There was not enough of " + product + " in the warehouse");
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
