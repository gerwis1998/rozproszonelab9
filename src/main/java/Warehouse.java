import java.util.HashMap;
import java.util.Map;

class Warehouse extends Thread{
    private final Map<Products, Integer> magazine;
    private final Integer max_capacity;

    Warehouse(Integer max_capacity){
        this.magazine = new HashMap<>();
        for (Products product: Products.values()) {
            magazine.put(product, 0);
        }
        this.max_capacity = max_capacity;
    }

    synchronized boolean ReceiveProduct(Products product, Integer amount){
        if(this.magazine.get(product) + amount <= this.max_capacity){
            this.magazine.put(product, this.magazine.get(product) + amount);
            System.out.println("Added " + amount + " x " + product + " to warehouse");
            return true;
        }
        else{
            System.out.println(product + " has reached max capacity");
            return false;
        }
    }

    synchronized boolean SellProduct(Products product, Integer amount){
        if(this.magazine.get(product) > amount){
            this.magazine.put(product, this.magazine.get(product) - amount);
            System.out.println("Removed " + amount + " x " + product + " from warehouse");
            return true;
        }
        else {
            System.out.println("There is not enough " + product + " in the warehouse");
            return false;
        }
    }
}
