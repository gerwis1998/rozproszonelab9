import java.util.ArrayList;
import java.util.List;

class Main {
    public static void main(String[] args){
        Warehouse warehouse = new Warehouse(100);
        List<Producer> producers = new ArrayList<>();
        for (int i=0; i < Products.values().length; i++){
            producers.add(new Producer(Products.values()[i], warehouse, i));
        }
        List<Client> clients = new ArrayList<>();
        for (int i=0; i < producers.size(); i++){
            clients.add(new Client(warehouse, i));
        }
        warehouse.start();
        for (Producer producer : producers){
            producer.start();
        }
        for (Client client : clients){
            client.start();
        }
        System.out.println("Main thread started");
    }
}
