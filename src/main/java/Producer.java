import java.util.Random;

class Producer extends Thread {
    private final Products product;
    private final Warehouse warehouse;
    private final Integer number;

    Producer(Products product, Warehouse warehouse, Integer number) {
        this.number = number;
        this.warehouse = warehouse;
        this.product = product;
    }

    @Override
    public void run() {
        System.out.println("Producer nr." + this.number + " of " + this.product + " is starting");
        Random generator = new Random();
        while (true) {
            try {
                sleep((generator.nextInt(10) + 1) * 1000);
                int amount = (generator.nextInt(5) + 1);
                boolean response = this.warehouse.ReceiveProduct(this.product, amount);
                if (response) {
                    System.out.println("Producer nr." + this.number + " produced " + amount + " of " + this.product);
                } else {
                    System.out.println("There was no place for " + amount + " x " + this.product);

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
